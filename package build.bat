MKDIR .\target
MKDIR .\target\package
DEL .\target\package\* /f /s /q
DEL brill-sync.zip /f /s /q

copy .\target\brill-sync-0.0.1-SNAPSHOT.jar .\target\package\brill-sync.jar
xcopy .\src\bundle\* .\target\package /e/i/h

jar -cMf .\target\brill-sync.zip -C .\target\package .\