/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech;

import com.genesisfeedtech.util.StringUtil;
import org.junit.Assert;
import org.junit.Test;

public class StringUtilTest {

    @Test
    public void quoteIsEscaped() {
        Assert.assertEquals("big \\\"B\\\"", StringUtil.jsonEscape("big \"B\""));
    }

    @Test
    public void backslashIsEscaped() {
        Assert.assertEquals("big\\\\small", StringUtil.jsonEscape("big\\small"));
    }

}