@echo off
rem
rem Copyright (c) Genesis Feed Technologies.  All rights reserved.
rem

set SERVICE_NAME=GFT_DataLink
set DESCRIPTION=Genesis Feed Tech Brill DataLink

set JAR=brill-sync.jar

set INSTALL_DIR=%cd%

IF EXIST nssm.exe del nssm.exe
IF $SYSTEM_os_arch==x86 (
    @echo 32 bit OS detected
    copy nssm32.exe nssm.exe
) ELSE (
    @echo 64 bit OS detected
    copy nssm64.exe nssm.exe
)

java -version

nssm install %SERVICE_NAME% StartDataLink.bat
nssm set %SERVICE_NAME% Description "%DESCRIPTION%"
nssm set %SERVICE_NAME% AppDirectory "%INSTALL_DIR%"
nssm start %SERVICE_NAME%

@echo done



