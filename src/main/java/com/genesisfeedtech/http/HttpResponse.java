/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.http;

public class HttpResponse {
    final int statusCode;
    final String responseBody;
    final Exception error;

    public HttpResponse(int statusCode, String responseBody) {
        this.statusCode = statusCode;
        this.responseBody = responseBody;
        this.error = null;
    }

    public HttpResponse(Exception e) {
        this.error = e;
        this.statusCode = -1;
        this.responseBody = null;
    }

    public boolean isSuccessful() {
        return statusCode == 200;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public Exception getError() {
        return error;
    }

    public String getErrorMessage() {
        return error == null ? null : error.getMessage();
    }

    @Override
    public String toString() {
        return "HttpResponse{" +
                "statusCode=" + statusCode +
                ", responseBody='" + responseBody + '\'' +
                ", error=" + error +
                '}';
    }
}
