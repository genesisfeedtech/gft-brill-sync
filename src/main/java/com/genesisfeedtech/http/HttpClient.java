/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.http;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NoHttpResponseException;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.NonRepeatableRequestException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLContext;
import java.io.*;

@Component
public class HttpClient {
    public static final String HTTPS = "https";

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${gftcore.host}")
    private String host;

    @Value("${gftcore.port}")
    private int port;

    @Value("${gftcore.username}")
    private String username;

    @Value("${gftcore.password}")
    private String password;

    private HttpHost target = null;
    private CredentialsProvider credsProvider = null;
    private AuthCache authCache = null;

    private void initHttpClient() {
        if (target == null) {
            target = new HttpHost(host, port, HTTPS);

            credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(
                    new AuthScope(target.getHostName(), target.getPort()),
                    new UsernamePasswordCredentials(username, password));

            authCache = new BasicAuthCache();
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(target, basicAuth);
        }
    }

    private CloseableHttpClient getHttpClientIgnoresSslErrors(CredentialsProvider credsProvider) {
        try {
            SSLContext sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, (certificate, authType) -> true).build();

            CloseableHttpClient client = HttpClients.custom()
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setDefaultCredentialsProvider(credsProvider)
                    .build();

            return client;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public HttpResponse doGet(String uri) {
        initHttpClient();
        try (CloseableHttpClient httpclient = getHttpClientIgnoresSslErrors(credsProvider)) {

            // Add AuthCache to the execution context
            HttpClientContext localContext = HttpClientContext.create();
            localContext.setAuthCache(authCache);

            HttpGet httpGet = new HttpGet(uri);

            log.info("Executing GET request: " + httpGet.getRequestLine());

            try (CloseableHttpResponse response = httpclient.execute(target, httpGet, localContext)) {
                StatusLine statusLine = response.getStatusLine();
                ByteArrayOutputStream bas = new ByteArrayOutputStream();
                response.getEntity().writeTo(bas);
                return new HttpResponse(statusLine.getStatusCode(), bas.toString());
            }
        } catch (Exception e) {
            return new HttpResponse(e);
        }
    }

    public boolean doPost(String uri) {
        BasicHttpEntity requestEntity = new BasicHttpEntity();
        requestEntity.setContent(new ByteArrayInputStream(new byte[0]));
        requestEntity.setContentLength(0);
        return doPost(uri, requestEntity);
    }

    public boolean doPost(String uri, HttpEntity requestEntity) {
        boolean success = false;
        initHttpClient();
        try (CloseableHttpClient httpclient = getHttpClientIgnoresSslErrors(credsProvider)) {

            // Add AuthCache to the execution context
            HttpClientContext localContext = HttpClientContext.create();
            localContext.setAuthCache(authCache);

            HttpPost httppost = new HttpPost(uri);
            httppost.setEntity(requestEntity);

            log.info("Executing POST request: {}:{}{}", host, port, httppost.getURI());

            try (CloseableHttpResponse response = httpclient.execute(target, httppost, localContext)) {
                StatusLine statusLine = response.getStatusLine();
                if (statusLine.getStatusCode() != 200) {
                    log.error("Request {}\nfailed: {}\n{} ", httppost.getRequestLine(), statusLine, statusLine.getReasonPhrase());
                    log.error("Response was: {}", EntityUtils.toString(response.getEntity()));
                } else {
                    success = true;
                    log.debug("{}", statusLine);
                }
            } catch (IOException e) {
                log.error("Request Failed, request was {}", uri, e);
            }
        } catch (NoHttpResponseException e) {
            log.error("No response", e);
        } catch (IOException e) {
            log.error("Failed to clean up request",e);
        }

        return success;
    }

}
