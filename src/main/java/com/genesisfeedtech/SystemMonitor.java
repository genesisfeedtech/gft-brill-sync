/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class SystemMonitor implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(SyncTool.class);
    public static final String MARKERFILE = "DataLinkIsRunning";

    public void testForMarkerFile() {
        if (new File(MARKERFILE).exists()) {
            log.debug("Marker file {} found", MARKERFILE);
        } else {
            log.info("Marker file {} not found, shutting down", MARKERFILE);
            System.exit(0);
        }
    }

    public void createMarkerFile() {
        try {
            File runMarker = new File(MARKERFILE);
            if (!runMarker.createNewFile()) {
                log.warn("Marker file {} already exists", MARKERFILE);
            }
            runMarker.deleteOnExit();
        } catch (IOException e) {
            log.error("Failed to create marker file "+MARKERFILE,e);
        }
    }

    public void deleteMarkerFile() {
        File runMarker = new File(MARKERFILE);
        if (runMarker.delete()) {
            log.info("Marker file {} deleted", MARKERFILE);
        } else {
            log.warn("Marker file {} not deleted", MARKERFILE);
        }
    }

    @Override
    public void run() {
        log.info("Starting up");
        Thread.currentThread().setName("System Monitor");
        createMarkerFile();
        try {
            while (true) {
                Thread.sleep(1000);
                testForMarkerFile();
            }
        }catch (InterruptedException e) {
            log.warn("Sleep interrupted, exiting");
        }
    }
}
