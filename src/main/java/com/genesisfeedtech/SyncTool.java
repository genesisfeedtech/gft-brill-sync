/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech;

import com.genesisfeedtech.datareader.*;
import com.genesisfeedtech.datareader.StoredFormula.StoredFormulaV2;
import com.genesisfeedtech.datareader.general.*;
import com.genesisfeedtech.http.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SyncTool {
    private static final Logger log = LoggerFactory.getLogger(SyncTool.class);

    private String syncStarted = "/api/v1/integration/current/syncStarted";
    private String syncComplete = "/api/v1/integration/current/syncComplete";

    @Autowired
    private HttpClient httpClient;

    @Autowired
    PlantCode plantCode;

    @Autowired
    UserCode userCode;

    @Autowired
    FormulaSpec formulaSpec;

    @Autowired
    FormulaSpecIngredient formulaSpecIngredient;

    @Autowired
    FormulaSpecNutrient formulaSpecNutrient;

    @Autowired
    IngredientDefinition ingredientDefinition;

    @Autowired
    IngredientNutrient ingredientNutrient;

    @Autowired
    IngredientPrices ingredientPrices;

    @Autowired
    ProductionFormula productionFormula;

    @Autowired
    Solution solution;

    @Autowired
    SolutionFormula solutionFormula;

    @Autowired
    SolutionFormulaComboGroup solutionFormulaComboGroup;

    @Autowired
    SolutionFormulaIngredient solutionFormulaIngredient;

    @Autowired
    SolutionFormulaNutrient solutionFormulaNutrient;

    @Autowired
    SolutionGlobal solutionGlobal;

    @Autowired
    SolutionGlobalGroup solutionGlobalGroup;

    @Autowired
    SolutionPlant solutionPlant;

    @Autowired
    SolutionPlantGroup solutionPlantGroup;

    @Autowired
    StoredFormulaV2 storedFormula;

    @Autowired
    Configuration config;

    private long nextExecutionTime = 0;
    private boolean isUpdate = false;

    @Scheduled(fixedDelay = 1000)
    public synchronized void run() {
        long start = System.currentTimeMillis();
        if (start >= nextExecutionTime) {
            log.info("Sync started");
            config.update();
            if (config.isLoaded()) {
                reset();
                httpClient.doPost(syncStarted);
                track(plantCode.execute());
                track(userCode.execute());
                track(formulaSpec.execute());
                track(formulaSpecIngredient.execute());
                track(formulaSpecNutrient.execute());
                track(ingredientDefinition.execute());
                track(ingredientNutrient.execute());
                track(ingredientPrices.execute());
                track(productionFormula.execute());
                track(solution.execute());
                track(solutionFormula.execute());
                track(solutionFormulaComboGroup.execute());
                track(solutionFormulaIngredient.execute());
                track(solutionFormulaNutrient.execute());
                track(solutionGlobal.execute());
                track(solutionGlobalGroup.execute());
                track(solutionPlant.execute());
                track(solutionPlantGroup.execute());
                track(storedFormula.execute());

                httpClient.doPost(syncComplete);

                log.info("Sync completed in " + (System.currentTimeMillis() - start) + "ms.");
            } else {
                log.error("Failed to load remote config, halt sync!");
            }

            nextExecutionTime = System.currentTimeMillis() + (config.getSyncIntervalSeconds() * 1000L);
        } else {
            log.debug("next execution time in {}ms", (nextExecutionTime - start));
        }
    }

    private void reset() {
        isUpdate = false;
    }

    private void track(ExecutionResult result) {
        isUpdate = isUpdate || !result.nothingToPost;
    }
}
