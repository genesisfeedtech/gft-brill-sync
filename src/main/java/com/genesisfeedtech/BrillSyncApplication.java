/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.File;
import java.io.IOException;

@SpringBootApplication
@EnableScheduling
public class BrillSyncApplication {
	private static final Logger log = LoggerFactory.getLogger(BrillSyncApplication.class);

	public static void main(String[] args) {
		log.info("main");
		String mode = args.length >= 1 ? args[0] : "start";
		if ("start".equals(mode)) {
			start(args);
		} else if ("stop".equals(mode)) {
			stop(args);
		} else {
			log.error("Invalid mode "+mode);
		}
	}

	static synchronized void start(final String [] args){
		log.info("Starting process");
		SpringApplication.run(BrillSyncApplication.class, args);

		Thread thread = new Thread(new SystemMonitor());
		thread.setDaemon(false);
		thread.run();
	}

	static synchronized void stop(String [] args) {
		log.info("Shutting down");
		new SystemMonitor().deleteMarkerFile();
	}


}
