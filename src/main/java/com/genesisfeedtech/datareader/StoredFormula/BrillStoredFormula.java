/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.StoredFormula;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Map;

public class BrillStoredFormula {
    public String plantCode;
    public String formulaCode;
    public Number versionNumber;
    public String description;
    public Number batchWeight;
    public Number costWhenStored;
    public Number speciesCode;
    public Timestamp storeDate;
    public String logUser;
    public Timestamp logDate;
    public String formulaId;
    public String pricingPlant;
    public Timestamp effectiveDate;
    public Timestamp expirationDate;
    public String formulaSpecId;
    public String comment;
    public Collection<BrillStoredFormulaIngredient> ingredients;
    public Collection<BrillStoredFormulaNutrient> nutrients;

    public BrillStoredFormula(Map<String, Object> queryData) {
        plantCode = (String) queryData.get("PlantCode");
        formulaCode = (String) queryData.get("FormulaCode");
        versionNumber = (Number) queryData.get("Version");
        description = (String) queryData.get("Description");
        batchWeight = (Number) queryData.get("BatchWeight");
        costWhenStored = (Number) queryData.get("CostWhenStored");
        speciesCode = (Number) queryData.get("SpeciesCode");
        storeDate = (Timestamp) queryData.get("StoreDate");
        logUser = (String) queryData.get("LogUser");
        logDate = (Timestamp) queryData.get("LogDate");
        formulaId = (String) queryData.get("FormulaID");
        pricingPlant = (String) queryData.get("PricingPlant");
        effectiveDate = (Timestamp) queryData.get("EffectiveDate");
        expirationDate = (Timestamp) queryData.get("ExpirationDate");
        formulaSpecId = (String) queryData.get("FormulaSpecID");
        comment = (String) queryData.get("Comment");
    }
}
