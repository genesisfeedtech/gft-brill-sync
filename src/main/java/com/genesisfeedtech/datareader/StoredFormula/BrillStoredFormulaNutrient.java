/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.StoredFormula;

import java.util.Map;


public class BrillStoredFormulaNutrient {

    public String formulaId;
    public String code;
    public String description;
    public Number amount;
    public String units;

    public BrillStoredFormulaNutrient(Map<String, Object> queryData) {
        formulaId = (String) queryData.get("FormulaID");
        code = (String) queryData.get("Code");
        description = (String) queryData.get("Description");
        amount = (Number) queryData.get("Amount");
        units = (String) queryData.get("Units");
    }
}
