/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.StoredFormula;

import com.genesisfeedtech.Configuration;
import com.genesisfeedtech.datareader.ExecutionResult;
import com.genesisfeedtech.datareader.general.QueryWithParameters;
import com.genesisfeedtech.http.HttpClient;
import com.genesisfeedtech.util.Checkpoint;
import com.google.common.base.Charsets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class StoredFormulaV2  {
    public static final int ONEMB = 1024 * 1024;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String API = "/api/integration/brill/storedFormula/create";

    @Autowired
    Configuration config;

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private HttpClient httpClient;

    private Timestamp lastLogDate = new Timestamp(0);
    private int currentRowCount;
    private int lastRowCount;

    private List<BrillStoredFormula> batch;
    private int batchSize = 2;
    private boolean readFailed;
    private boolean postFailed;
    private boolean nothingToPost;


    private QueryWithParameters getStoredFormulaQuery() {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("lastLog", lastLogDate);
        StringBuilder queryString = new StringBuilder(config.getBaseStoredFormulaQuery());
        if (queryString.toString().toLowerCase(Locale.US).contains("where")) {
            queryString.append(" AND ");
        } else {
            queryString.append(" WHERE ");
        }
        queryString.append(" LogDate >= :lastLog ");
        if (config.restrictByPlantCode()) {
            parameters.putAll(config.getRestrictions());
            queryString.append(" AND PlantCode IN (:"+config.PlantCodeRestriction+")");
        }

        queryString.append(" ORDER BY LogDate ASC");
        return new QueryWithParameters(queryString.toString(), parameters);
    }

    private QueryWithParameters getStoredFormulaIngredientQuery(BrillStoredFormula formula) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("formulaId", formula.formulaId);
        StringBuilder queryString = new StringBuilder(config.getBaseStoredFormulaIngredientQuery());
        if (queryString.toString().toLowerCase(Locale.US).contains("where")) {
            queryString.append(" AND ");
        } else {
            queryString.append(" WHERE ");
        }
        queryString.append(" FormulaId = :formulaId ");

        return new QueryWithParameters(queryString.toString(), parameters);
    }

    private QueryWithParameters getStoredFormulaNutrientQuery(BrillStoredFormula formula) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("formulaId", formula.formulaId);
        StringBuilder queryString = new StringBuilder(config.getBaseStoredFormulaNutrientQuery());
        if (queryString.toString().toLowerCase(Locale.US).contains("where")) {
            queryString.append(" AND ");
        } else {
            queryString.append(" WHERE ");
        }
        queryString.append(" FormulaId = :formulaId ");

        return new QueryWithParameters(queryString.toString(), parameters);
    }


    public ExecutionResult execute() {
        log.debug("Begin query");
        Checkpoint cp = new Checkpoint();
        List<Map<String, Object>> formulas = null;
        try {
            QueryWithParameters query = getStoredFormulaQuery();
            log.debug("Executing query {}", query);
            formulas = jdbcTemplate.queryForList(query.query, query.parameters);
            cp.add("formulaQuery");
            readFailed = false;
            currentRowCount = formulas.size();
            nothingToPost = currentRowCount == lastRowCount;

            if (nothingToPost) {
                log.debug("Nothing to post");
            } else {
                lastRowCount = currentRowCount;
            }

            batch = new ArrayList<>(batchSize);
            for (Map<String, Object> formulaData : formulas) {
                Checkpoint cp2 = new Checkpoint();
                BrillStoredFormula storedFormula = new BrillStoredFormula(formulaData);

                QueryWithParameters ingredientQuery = getStoredFormulaIngredientQuery(storedFormula);
                List<Map<String, Object>> ingredients = jdbcTemplate.queryForList(ingredientQuery.query, ingredientQuery.parameters);
                storedFormula.ingredients = new ArrayList<>(ingredients.size());
                for (Map<String, Object> ingredientData : ingredients) {
                    BrillStoredFormulaIngredient ingredient = new BrillStoredFormulaIngredient(ingredientData);
                    storedFormula.ingredients.add(ingredient);
                }
                cp2.add("ingredients");

                QueryWithParameters nutrientQuery = getStoredFormulaNutrientQuery(storedFormula);
                List<Map<String, Object>> nutrients = jdbcTemplate.queryForList(nutrientQuery.query, nutrientQuery.parameters);
                storedFormula.nutrients = new ArrayList<>(nutrients.size());
                for (Map<String, Object> nutrientData: nutrients) {
                    BrillStoredFormulaNutrient nutrient = new BrillStoredFormulaNutrient(nutrientData);
                    storedFormula.nutrients.add(nutrient);
                }
                cp2.add("nutrients");

                batch.add(storedFormula);
                if (batch.size() >= batchSize) {
                    postResults();
                    if (postFailed) {
                        log.warn("Post failed, abort sync");
                        return executionResult();
                    }
                    cp2.add("post");
                }
                log.debug("handle StoredFormula {}", cp2);
            }
            postResults();
            cp.add("process " + formulas.size());

            readFailed = false;
        } catch (DataAccessException e) {
            log.error("Read error - check sql connection configuration", e);
            readFailed = true;
        } finally {
            log.info("Process complete, {} rows: {}", currentRowCount, cp);
        }

        return executionResult();
    }

    public ExecutionResult executionResult() {
        return new ExecutionResult(readFailed, postFailed, nothingToPost);
    }

    private void postResults() {
        if (batch.isEmpty()) {
            log.debug("postResults: Nothing to do");
            return;
        }
        try {
            String json = getBatchJson();
            if (json.length() > ONEMB) {
                log.warn("POST {} bytes", json.length());
            } else {
                log.debug("POST {} bytes", json.length());
            }
            StringEntity reqEntity = new StringEntity(json, Charsets.UTF_8);
            reqEntity.setChunked(true);
            reqEntity.setContentType("application/json");

            boolean success = httpClient.doPost(API, reqEntity);
            if (success) {
                lastLogDate = batch.get(batch.size() - 1).logDate;
                lastRowCount = -1;
                batch = new ArrayList<>(batchSize);
                log.debug("postResults: batch success, lastLogDate is "+lastLogDate);
            } else {
                log.warn("postResults: batch failed");
            }
            postFailed = !success;
        } catch (Exception e) {
            postFailed = true;
            log.error("Exception thrown posting batch {}", e);
        }
    }

    protected String getBatchJson() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").create();

        return gson.toJson(batch);
    }
}
