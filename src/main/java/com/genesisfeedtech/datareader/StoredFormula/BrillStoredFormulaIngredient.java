/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.StoredFormula;

import java.util.Map;

public class BrillStoredFormulaIngredient {
    public String formulaId;
    public String code;
    public String description;
    public Number amount;
    public String batchCode;
    public String groupNumber;
    public String binNumber;
    public Number cost;
    public Number costPerUnit;
    public String ingrDefinitionId;
    public String premixFormulaCode;
    public Boolean inStock;

    public BrillStoredFormulaIngredient(Map<String, Object> queryData) {
        formulaId = (String) queryData.get("FormulaID");
        code = (String) queryData.get("Code");
        description = (String) queryData.get("Description");
        amount = (Number) queryData.get("Amount");
        batchCode = (String) queryData.get("BatchCode");
        groupNumber = (String) queryData.get("GroupNumber");
        binNumber = (String) queryData.get("BinNumber");
        cost = (Number) queryData.get("Cost");
        costPerUnit = (Number) queryData.get("CostPerUnit");
        ingrDefinitionId = (String) queryData.get("IngrDefinitionID");
        premixFormulaCode = (String) queryData.get("PremixFormulaCode");
        inStock = (Boolean) queryData.get("InStock");
    }
}
