/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.general;

import com.genesisfeedtech.util.StringUtil;
import com.genesisfeedtech.datareader.ExecutionResult;
import com.genesisfeedtech.http.HttpClient;
import com.google.common.base.Charsets;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

public abstract class AbstractDataReader {
    protected static final String SKIP = "!!SKIP!!";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
    public static final int ONEMB = 1024 * 1024;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private HttpClient httpClient;

    private final String table;

    private final String apiPath;
    private final Map<String, String> mappings;
    private int lastRowCount = 0;
    private int currentRowCount = 0;
    private boolean postFailed = false;
    private boolean readFailed = false;

    protected AbstractDataReader(String table, String apiPath, Map<String, String> mappings) {
        this.table = table;
        this.apiPath = apiPath;
        this.mappings = mappings;
    }

    protected abstract QueryWithParameters getQuery();

    public ExecutionResult execute() {
        long start = System.currentTimeMillis();
        readData();
        boolean dataToPost = currentRowCount > lastRowCount;
        if (!readFailed && (postFailed || dataToPost)) {
            postResults();
            lastRowCount = currentRowCount;
        } else {
            log.debug("Nothing to POST");
        }
        long end = System.currentTimeMillis();

        log.info("{} Done in {}ms.", table, (end - start));
        return new ExecutionResult(readFailed, postFailed, !dataToPost);
    }

    private void postResults() {
        try {
            File file = new File(table);
            String content = new String(Files.readAllBytes(file.toPath()));
            log.debug("File {} read into string, len {} bytes", file, content.length());
            if (content.length() > ONEMB) {
                log.warn("File {} read into string, len {} bytes", file, content.length());
            }
            StringEntity reqEntity = new StringEntity(content, Charsets.UTF_8);
            reqEntity.setChunked(true);
            reqEntity.setContentType("application/json");

            boolean success = httpClient.doPost(apiPath, reqEntity);
            postFailed = !success;
        } catch (IOException e) {
            log.error("Failed to open file "+table, e);
        }
    }

    private void readData() {
        log.debug("{} Begin query", table);
        List<Map<String, Object>> queryResult = null;
        try {
            QueryWithParameters query = getQuery();
            log.debug("Executing query {}", query);
            queryResult = jdbcTemplate.queryForList(query.query, query.parameters);

            currentRowCount = queryResult.size();
            log.debug("{} Query complete, {} rows", table, currentRowCount);

            if (currentRowCount > lastRowCount) {
                writeToFile(queryResult);
            }
            readFailed = false;
        } catch (DataAccessException e) {
            log.error("Read error - check sql connection configuration", e);
            readFailed = true;
        } catch(FileNotFoundException e) {
            log.error("Cannot open file ", e);
            readFailed = true;
        }
    }

    private void writeToFile(List<Map<String, Object>> queryResult) throws FileNotFoundException {
        try (PrintWriter file = new PrintWriter(table)) {
            file.print("[");
            boolean firstRow = true;
            for (Map<String, Object> row : queryResult) {
                if (firstRow) {
                    firstRow = false;
                } else {
                    file.print(", ");
                }
                file.print("{");
                boolean firstKey = true;
                for (String key : row.keySet()) {
                    if (mappings.get(key) == SKIP) {
                        continue;
                    }
                    if (firstKey) {
                        firstKey = false;
                    } else {
                        file.print(", ");
                    }

                    file.print("\"");
                    if (mappings.containsKey(key)) {
                        file.print(mappings.get(key));
                    } else {
                        file.print(camelCase(key));
                    }
                    file.print("\": ");

                    Object value = row.get(key);
                    if (value == null) {
                        file.print("null");
                    } else if (value instanceof Number) {
                        file.print(value);
                    } else if (value instanceof Boolean) {
                        file.print(value);
                    } else if (value instanceof String) {
                        file.print("\"");
                        file.print(StringUtil.jsonEscape((String) value));
                        file.print("\"");
                    } else if (value instanceof Timestamp) {
                        file.print("\"");
                        file.print(DATE_FORMAT.format(value));
                        file.print("\"");
                    } else {
                        Class<?> valueClass = value.getClass();
                        throw new RuntimeException("Unexpected datatype for column " + key + ": " + valueClass);
                    }
                }
                file.print("}");
            }

            file.print("]");
        }

        log.debug("{} to json complete", table);
    }

    private String camelCase(String input) {
        return input.substring(0, 1).toLowerCase()+input.substring(1);
    }
}
