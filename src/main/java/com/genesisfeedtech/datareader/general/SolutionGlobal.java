/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.general;

import com.genesisfeedtech.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class SolutionGlobal extends AbstractDataReader {
    private static final String TABLE = "FMS_SolutionGlobal";
    private static final String API = "/api/integration/brill/solutionGlobal/add";
    private static final Map<String, String> MAPPINGS = new HashMap<String, String>(){{
        put("SolutionID", "solutionId");
    }};

    @Autowired
    Configuration config;

    public SolutionGlobal() {
        super(TABLE, API, MAPPINGS);
    }

    @Override
    public QueryWithParameters getQuery() {
        return config.getSolutionGlobalQuery();
    }
}
