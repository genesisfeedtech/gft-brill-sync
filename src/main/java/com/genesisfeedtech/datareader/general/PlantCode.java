/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.general;

import com.genesisfeedtech.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class PlantCode extends AbstractDataReader {
    private static final String TABLE = "PlantCode";
    private static final String API = "/api/integration/brill/integration/addPlants";
    private static final Map<String, String> MAPPINGS = new HashMap<String, String>(){{
        put("PlantCode", "code");
    }};

    @Autowired
    Configuration config;

    public PlantCode() {
        super(TABLE, API, MAPPINGS);
    }

    @Override
    public QueryWithParameters getQuery() {
        return config.getPlantCodeQuery();
    }
}
