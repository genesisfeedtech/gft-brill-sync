/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.general;

import com.genesisfeedtech.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserCode extends AbstractDataReader {
    private static final String TABLE = "UserCode";
    private static final String API = "/api/integration/brill/integration/addUsers";
    private static final Map<String, String> MAPPINGS = new HashMap<String, String>(){{
        put("UserName", "code");
    }};

    @Autowired
    Configuration config;

    public UserCode() {
        super(TABLE, API, MAPPINGS);
    }

    @Override
    public QueryWithParameters getQuery() {
        return config.getUserCodeQuery();
    }
}
