/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader.general;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.util.Map;

public class QueryWithParameters {

    public final String query;

    public final MapSqlParameterSource parameters = new MapSqlParameterSource();

    public QueryWithParameters(String query) {
        this.query = query;
    }

    public QueryWithParameters(String query, Map<String, Object> namedParameters) {
        this.query = query;
        if (namedParameters != null) {
            for (Map.Entry<String, Object> entry : namedParameters.entrySet()) {
                parameters.addValue(entry.getKey(), entry.getValue());
            }
        }
    }

    @Override
    public String toString() {
        return "QueryWithParameters{" +
                "query='" + query + '\'' +
                ", parameters=" + parameters.getValues() +
                '}';
    }
}
