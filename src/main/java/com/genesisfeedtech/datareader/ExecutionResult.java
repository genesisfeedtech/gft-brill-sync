/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.datareader;

public class ExecutionResult {

    public final boolean readFailed;
    public final boolean postFailed;
    public final boolean completedSuccessfully;
    public final boolean nothingToPost;

    public ExecutionResult(boolean readFailed, boolean postFailed, boolean nothingToPost) {
        this.readFailed = readFailed;
        this.postFailed = postFailed;
        this.nothingToPost = nothingToPost;
        this.completedSuccessfully = !readFailed && !postFailed;
    }
}
