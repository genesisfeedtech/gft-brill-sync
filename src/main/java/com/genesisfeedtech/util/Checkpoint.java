/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.util;

import java.util.ArrayList;
import java.util.List;

public class Checkpoint {
    private List<Entry> entries = new ArrayList<>();
    private Entry start;

    public Checkpoint() {
        start = new Entry("START", now());
    }

    public void add(String name) {
        entries.add(new Entry(name, now()));
    }

    protected long now() {
        return System.currentTimeMillis();
    }

    @Override
    public String toString() {
        long now = now();

        StringBuilder builder = new StringBuilder("(").append("total: ").append(now-start.timestamp).append("ms) | ");

        Entry previous = start;
        for (Entry e : entries) {
            builder.append(e.name).append(": ").append(e.timestamp - previous.timestamp).append("ms | ");
            previous = e;
        }

        return builder.toString();
    }

    private static class Entry {
        long timestamp;
        String name;

        public Entry(String name, long timestamp) {
            this.timestamp = timestamp;
            this.name = name;
        }
    }
}


