/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech.util;

public class StringUtil {
    public static String jsonEscape(String value) {
        StringBuilder result = new StringBuilder();
        for (char c : value.toCharArray()) {

            switch (c) {
                case 0x08:
                    result.append("\\b");
                    break;
                case 0x0C:
                    result.append("\\f");
                    break;
                case '\n':
                    result.append("\\n");
                    break;
                case '\r':
                    result.append("\\r");
                    break;
                case '\t':
                    result.append("\\t");
                    break;
                case '\"':
                    result.append("\\\"");
                    break;
                case '\\':
                    result.append("\\\\");
                    break;
                default:
                    result.append(c);
            }
        }
        return result.toString();
    }


}
