/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech;

import com.genesisfeedtech.datareader.general.QueryWithParameters;
import com.genesisfeedtech.http.HttpClient;
import com.genesisfeedtech.http.HttpResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Scope("singleton")
public class Configuration {
    public static final String NONE = "NoneAllowedDoNotMatchAnythingMakeThisStringLongerThanFiftyCharacters";

    public static final String CONFIGOPTION_ALLOWED_PLANT_CODES = "allowedPlantCodes";
    public static final String CONFIGOPTION_ALLOWED_USERNAMES = "allowedUserCodes";
    public static final String PlantCodeRestriction = "plantCodes";
    public static final String UsernameRestriction = "usernames";
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private String apiPath = "/api/v1/integration/current/detail";

    @Value("${datalink.heartbeat-interval-seconds ?: 60}")
    private long heartbeatIntervalSeconds;

    @Value("${datalink.sync-interval-seconds ?: 60}")
    private long syncIntervalSeconds;

    @Value("${formulaSpec.query ?: SELECT * FROM FMS_FormulaSpec}")
    private String formulaSpecQuery;

    @Value("${formulaSpecIngredient.query ?: SELECT * FROM FMS_FormulaSpecIngredient}")
    private String formulaSpecIngredientQuery;

    @Value("${formulaSpecNutrient.query ?: SELECT * FROM FMS_FormulaSpecNutrient}")
    private String formulaSpecNutrientQuery;

    @Value("${ingredientDefinition.query ?: SELECT * FROM FMS_IngredientDefinition}")
    private String ingredientDefinitionQuery;

    @Value("${ingredientNutrient.query ?: SELECT * FROM FMS_IngredientNutrient}")
    private String ingredientNutrientQuery;

    @Value("${ingredientPrices.query ?: SELECT * FROM FMS_IngredientPrices}")
    private String ingredientPricesQuery;

    @Value("${productionFormula.query ?: SELECT * FROM FMS_ProductionFormula}")
    private String productionFormulaQuery;

    @Value("${solution.query ?: SELECT * FROM FMS_Solution}")
    private String solutionQuery;

    @Value("${solutionFormula.query ?: SELECT * FROM FMS_SolutionFormula}")
    private String solutionFormulaQuery;

    @Value("${solutionFormulaComboGroup.query ?: SELECT * FROM FMS_SolutionFormulaComboGroup}")
    private String solutionFormulaComboGroupQuery;

    @Value("${solutionFormulaIngredient.query ?: SELECT * FROM FMS_SolutionFormulaIngredient}")
    private String solutionFormulaIngredientQuery;

    @Value("${solutionFormulaNutrient.query ?: SELECT * FROM FMS_SolutionFormulaNutrient}")
    private String solutionFormulaNutrientQuery;

    @Value("${solutionGlobal.query ?: SELECT * FROM FMS_SolutionGlobal}")
    private String solutionGlobalQuery;

    @Value("${solutionGlobalGroup.query ?: SELECT * FROM FMS_SolutionGlobalGroup}")
    private String solutionGlobalGroupQuery;

    @Value("${solutionPlant.query ?: SELECT * FROM FMS_SolutionPlant}")
    private String solutionPlantQuery;

    @Value("${solutionPlantGroup.query ?: SELECT * FROM FMS_SolutionPlantGroup}")
    private String solutionPlantGroupQuery;

    @Value("${storedFormula.query ?: SELECT * FROM FMS_StoredFormula}")
    private String storedFormulaQuery;

    @Value("${storedFormulaIngredient.query ?: SELECT * FROM FMS_StoredFormulaIngredient}")
    private String storedFormulaIngredientQuery;

    @Value("${storedFormulaNutrient.query ?: SELECT * FROM FMS_StoredFormulaNutrient}")
    private String storedFormulaNutrientQuery;

    @Value("${plantCode.query ?: SELECT PlantCode FROM FMS_FormulaSpec UNION SELECT PlantCode FROM FMS_IngredientDefinition UNION SELECT PlantCode FROM FMS_IngredientPrices UNION SELECT PlantCode FROM FMS_ProductionFormula UNION SELECT PlantCode FROM FMS_StoredFormula}")
    private String plantCodeQuery;

    @Value("${userCode.query ?: SELECT UserName FROM FMS_Solution UNION SELECT LogUser from FMS_IngredientDefinition SELECT LogUser from FMS_IngredientPrices SELECT LogUser from FMS_StoredFormula}")
    private String userCodeQuery;

    @Autowired
    private HttpClient httpClient;

    private JSONParser parser = new JSONParser();

    private Map<String, String> remoteConfig = new HashMap<>();

    private Set<String> plantCodes = null;
    private Set<String> usernames = null;
    
    private Map<String, Object> restrictions = new HashMap<>();
    private boolean isLoaded = false;

    public synchronized void update() {
        log.info("Get current configuration");
        isLoaded = false;

        HttpResponse response = httpClient.doGet(apiPath);
        log.debug("GET {}\n{}", apiPath, response);
        if (response.isSuccessful()) {
            try {
                Map<String, String> config = new HashMap<>();
                JSONObject o = (JSONObject)parser.parse(response.getResponseBody());
                JSONArray configOptions = (JSONArray) o.get("configOptions");
                for (Object entry : configOptions) {
                    String name = (String) ((JSONObject) entry).get("name");
                    String value = (String) ((JSONObject) entry).get("value");
                    config.put(name, value);
                    log.info("Remote config: {} => {}", name, value);
                }
                remoteConfig = Collections.unmodifiableMap(config);
                updateRestrictions();
                isLoaded = true;
                log.info("Successfully loaded remote configuration");
            } catch (ParseException e) {
                log.error("{} reading remote configuration!  Response was \n{}", e, response.getResponseBody());

            }
        } else {
            log.error("Failed to get current configuration: {}", response.getErrorMessage());
        }
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    private void updateRestrictions() {
        restrictions = new HashMap<>();

        if (restrictByPlantCode()) {
            plantCodes = getRestriction(CONFIGOPTION_ALLOWED_PLANT_CODES);
        } else {
            plantCodes = Collections.EMPTY_SET;
        }
        restrictions.put(PlantCodeRestriction, plantCodes);

        if (restrictByUsername()) {
            usernames = getRestriction(CONFIGOPTION_ALLOWED_USERNAMES);
        } else {
            usernames = Collections.EMPTY_SET;
        }
        restrictions.put(UsernameRestriction, usernames);
    }

    public Map<String, Object> getRestrictions() {
        return Collections.unmodifiableMap(restrictions);
    }

    private Set<String> getRestriction(String configOption) {
        Set<String> result = new HashSet<>();
        String codes = remoteConfig.get(configOption);
        if (codes != null) {
            for (String s : codes.split(",")) {
                String code = s.trim();
                if (!code.isEmpty()) {
                    result.add(code);
                }
            }
        }

        if (result.isEmpty()) {
            result.add(NONE);
        }
        return result;
    }

    public boolean restrictByPlantCode() {
        String s = remoteConfig.get(CONFIGOPTION_ALLOWED_PLANT_CODES);
        return !"*".equals(s);
    }

    private boolean restrictByUsername() {
        String s = remoteConfig.get(CONFIGOPTION_ALLOWED_USERNAMES);
        return !"*".equals(s);
    }

    private synchronized void validate() {
        if (!isLoaded) {
            throw new InvalidConfigurationException("Remote configuration not loaded");
        }
    }

    public long getHeartbeatIntervalSeconds() {
        if (!isLoaded) {
            log.warn("remote configuration has not been loaded, using local defaults for HeartbeatIntervalSeconds");
        } else {
            String remoteHeartbeatIntervalMinutes = remoteConfig.get("heartbeatIntervalMinutes");
            if (remoteHeartbeatIntervalMinutes != null) {
                return Long.parseLong(remoteHeartbeatIntervalMinutes) * 60;
            }
        }

        return heartbeatIntervalSeconds;
    }

    public long getSyncIntervalSeconds() {
        if (!isLoaded) {
            log.warn("remote configuration has not been loaded, using local defaults for SyncIntervalSeconds");
        } else {
            String remoteSyncIntervalSeconds = remoteConfig.get("syncIntervalSeconds");
            if (remoteSyncIntervalSeconds != null) {
                return Long.parseLong(remoteSyncIntervalSeconds);
            }
        }

        return syncIntervalSeconds;
    }

    public QueryWithParameters getFormulaSpecQuery() {
        validate();
        String remoteQuery = remoteConfig.get("formulaSpecQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT * FROM FMS_FormulaSpec WHERE PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(formulaSpecQuery);
    }

    public QueryWithParameters getFormulaSpecIngredientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("formulaSpecIngredientQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT i.* FROM FMS_FormulaSpecIngredient i JOIN FMS_FormulaSpec f on i.FormulaSpecID = f.FormulaSpecID where f.PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(formulaSpecIngredientQuery);
    }

    public QueryWithParameters getFormulaSpecNutrientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("formulaSpecNutrientQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT n.* FROM FMS_FormulaSpecNutrient n JOIN FMS_FormulaSpec f on n.FormulaSpecID = f.FormulaSpecID where f.PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(formulaSpecNutrientQuery);
    }

    public QueryWithParameters getIngredientDefinitionQuery() {
        validate();
        String remoteQuery = remoteConfig.get("ingredientDefinitionQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT * FROM FMS_IngredientDefinition WHERE PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(ingredientDefinitionQuery);
    }

    public QueryWithParameters getIngredientNutrientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("ingredientNutrientQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT n.* FROM FMS_IngredientNutrient n JOIN FMS_IngredientDefinition i ON n.IngrDefinitionID = i.IngrDefinitionID WHERE i.PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(ingredientNutrientQuery);
    }

    public QueryWithParameters getIngredientPricesQuery() {
        validate();
        String remoteQuery = remoteConfig.get("ingredientPricesQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT * FROM FMS_IngredientPrices WHERE PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(ingredientPricesQuery);
    }

    public QueryWithParameters getProductionFormulaQuery() {
        validate();
        String remoteQuery = remoteConfig.get("productionFormulaQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT * FROM FMS_ProductionFormula WHERE PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(productionFormulaQuery);
    }

    public QueryWithParameters getSolutionQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT * FROM FMS_Solution WHERE UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionQuery);
    }

    public QueryWithParameters getSolutionFormulaQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionFormulaQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT f.* FROM FMS_SolutionFormula f JOIN FMS_Solution s ON f.SolutionID = s.SolutionID WHERE s.UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionFormulaQuery);
    }

    public QueryWithParameters getSolutionFormulaComboGroupQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionFormulaComboGroupQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT c.* FROM FMS_SolutionFormulaComboGroup c JOIN FMS_SolutionFormula f ON c.FormulaID = f.FormulaID JOIN FMS_Solution s ON f.SolutionID = s.SolutionID WHERE s.UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionFormulaComboGroupQuery);
    }

    public QueryWithParameters getSolutionFormulaIngredientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionFormulaIngredientQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT i.* FROM FMS_SolutionFormulaIngredient i JOIN FMS_SolutionFormula f ON i.FormulaID = f.FormulaID JOIN FMS_Solution s ON f.SolutionID = s.SolutionID WHERE s.UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionFormulaIngredientQuery);
    }

    public QueryWithParameters getSolutionFormulaNutrientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionFormulaNutrientQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT n.* FROM FMS_SolutionFormulaNutrient n JOIN FMS_SolutionFormula f ON n.FormulaID = f.FormulaID JOIN FMS_Solution s ON f.SolutionID = s.SolutionID WHERE s.UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionFormulaNutrientQuery);
    }

    public QueryWithParameters getSolutionGlobalQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionGlobalQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT g.* FROM FMS_SolutionGlobal g JOIN FMS_Solution s ON g.SolutionID = s.SolutionID WHERE s.UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionGlobalQuery);
    }

    public QueryWithParameters getSolutionGlobalGroupQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionGlobalGroupQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT g.* FROM FMS_SolutionGlobalGroup g JOIN FMS_Solution s ON g.SolutionID = s.SolutionID WHERE s.UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionGlobalGroupQuery);
    }

    public QueryWithParameters getSolutionPlantQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionPlantQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT p.* FROM FMS_SolutionPlant p JOIN FMS_Solution s ON p.SolutionID = s.SolutionID WHERE s.UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionPlantQuery);
    }

    public QueryWithParameters getSolutionPlantGroupQuery() {
        validate();
        String remoteQuery = remoteConfig.get("solutionPlantGroupQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByUsername()) {
            return new QueryWithParameters("SELECT p.* FROM FMS_SolutionPlantGroup p JOIN FMS_Solution s ON p.SolutionID = s.SolutionID WHERE s.UserName IN (:usernames)", restrictions);
        }

        return new QueryWithParameters(solutionPlantGroupQuery);
    }

    public QueryWithParameters getStoredFormulaQuery() {
        validate();
        String remoteQuery = remoteConfig.get("storedFormulaQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT * FROM FMS_StoredFormula WHERE PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(storedFormulaQuery);
    }

    public String getBaseStoredFormulaQuery() {
        validate();
        String remoteQuery = remoteConfig.get("storedFormulaQuery");
        if (remoteQuery != null) {
            return remoteQuery;
        } else {
            return storedFormulaQuery;
        }
    }

    public QueryWithParameters getStoredFormulaIngredientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("storedFormulaIngredientQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT i.* FROM FMS_StoredFormulaIngredient i JOIN FMS_StoredFormula f on i.FormulaID = f.FormulaID where f.PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(storedFormulaIngredientQuery);
    }

    public String getBaseStoredFormulaIngredientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("storedFormulaIngredientQuery");
        if (remoteQuery != null) {
            return remoteQuery;
        } else {
            return storedFormulaIngredientQuery;
        }
    }

    public QueryWithParameters getStoredFormulaNutrientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("storedFormulaNutrientQuery");
        if (remoteQuery != null) {
            return new QueryWithParameters(remoteQuery);
        }

        if (restrictByPlantCode()) {
            return new QueryWithParameters("SELECT n.* FROM FMS_StoredFormulaNutrient n JOIN FMS_StoredFormula f on n.FormulaID = f.FormulaID where f.PlantCode IN (:plantCodes)", restrictions);
        }

        return new QueryWithParameters(storedFormulaNutrientQuery);
    }

    public String getBaseStoredFormulaNutrientQuery() {
        validate();
        String remoteQuery = remoteConfig.get("storedFormulaNutrientQuery");
        if (remoteQuery != null) {
            return remoteQuery;
        } else {
            return storedFormulaNutrientQuery;
        }
    }

    public QueryWithParameters getPlantCodeQuery() {
        return new QueryWithParameters(plantCodeQuery);
    }

    public QueryWithParameters getUserCodeQuery() {
        return new QueryWithParameters(userCodeQuery);
    }
}
