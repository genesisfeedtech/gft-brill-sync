/*
 * Copyright (c) Genesis Feed Technologies.  All rights reserved.
 */

package com.genesisfeedtech;

import com.genesisfeedtech.http.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class HeartbeatService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final long ONE_DAY_SECONDS = 24 * 60 *60;

    private String apiPath = "/api/v1/integration/current/heartbeat";

    @Autowired
    Configuration config;

    private long nextHeartbeatTime = 0;

    @Autowired
    private HttpClient httpClient;

    @Scheduled(fixedDelay = 1000)
    public synchronized void doHeartbeat() {
        long start = System.currentTimeMillis();
        if (start >= nextHeartbeatTime) {
            log.info("Heartbeat started");
            httpClient.doPost(apiPath);
            long end = System.currentTimeMillis();
            log.info("Heartbeat completed in " + (end - start) + "ms.");
            long heartbeatIntervalSeconds = getHeartbeatIntervalSeconds();

            nextHeartbeatTime = end + (heartbeatIntervalSeconds * 1000L);
        } else {
            log.debug("next heartbeat in {}ms", (nextHeartbeatTime - start));
        }
    }

    private long getHeartbeatIntervalSeconds() {
        long seconds = config.getHeartbeatIntervalSeconds();
        if (seconds > ONE_DAY_SECONDS) {
            log.warn("Heartbeat interval exceeds 24 hours, using 24 hour heartbeat");
            return ONE_DAY_SECONDS;
        } else {
            return seconds;
        }

    }

}
